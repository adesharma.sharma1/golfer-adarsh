import styles from "./Test.module.css";

export const Test = () => {
  return (
    <div className={styles.backgroundColor}>
      <h1 className={styles.testHeader}>Welcome to Golfer! </h1>;
    </div>
  );
};
